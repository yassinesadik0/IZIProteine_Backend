<?php

use App\Http\Controllers\V1\OrderController;
use App\Http\Controllers\V1\PurchaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\V1\PhotoController;
use App\Http\Controllers\V1\PostController;
use App\Http\Controllers\V1\ProductController;
use App\Http\Controllers\V1\ProductVariantController;
use App\Http\Controllers\V1\RoleController;
use App\Http\Controllers\V1\UserController;
use App\Http\Controllers\V1\WasteController;
use App\Http\Controllers\V1\WasteTypeController;
use App\Http\Controllers\V1\StatisticController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(["prefix" => "v1"], function () {
	Route::group(["middleware" => "auth:sanctum"], function () {
		Route::apiResource("products", ProductController::class)->only(["store", "update", "destroy"]);
		Route::apiResource("productVariants", ProductVariantController::class)->only(["store", "update", "destroy"]);
		Route::apiResource("photos", PhotoController::class)->only(["store", "update", "destroy"]);
		Route::apiResource("posts", PostController::class)->only(["store", "update", "destroy"]);
		Route::apiResource("roles", RoleController::class)->only(["store", "update", "destroy"]);
		Route::apiResource("wastes", WasteController::class)->only(["store", "update", "destroy"]);
		Route::apiResource("wasteTypes", WasteTypeController::class)->only(["store", "update", "destroy"]);
		Route::apiResource("users", UserController::class)->only(["store", "update", "destroy"]);
		Route::apiResource("orders", OrderController::class)->only(["index", "show", "destroy", "update"]);
		Route::post("/logout", [UserController::class, "logout"])->name("users.logout");
		Route::post("/privateWasteStatistic", [StatisticController::class, "getSupplierRelatedStatistics"])->name("statistics.private");
	});
	
	Route::apiResource("products", ProductController::class)->only(["index", "show"]);
	Route::apiResource("productVariants", ProductVariantController::class)->only(["index", "show"]);
	Route::apiResource("photos", PhotoController::class)->only(["index", "show"]);
	Route::apiResource("posts", PostController::class)->only(["index", "show"]);
	Route::apiResource("roles", RoleController::class)->only(["index", "show"]);
	Route::apiResource("wastes", WasteController::class)->only(["index", "show"]);
	Route::apiResource("wasteTypes", WasteTypeController::class)->only(["index", "show"]);
	Route::apiResource("users", UserController::class)->only(["index", "show"]);

	Route::get("/publicWasteStatistic", [StatisticController::class, "getPublicWasteStatistics"])->name("statistics.public");
	Route::get("/searchUsers", [UserController::class, "searchUser"]);
	// old approach for purchasing mechanism
	// Route::post("purchase", [PurchaseController::class, "purchase"]);

	// for order tracking
	Route::post("orders", [OrderController::class,"store"]);

	Route::post("/login", [UserController::class, "login"])->name("users.login");
	Route::post("/register", [UserController::class, "register"])->name("users.register");
	Route::post("/checkToken", [UserController::class, "checkToken"])->name("users.checkToken");

	
	Route::get("/test", function () {
		return response()->json(["dfdsf" => "dsfsf"]);
	});
});

