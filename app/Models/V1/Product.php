<?php

namespace App\Models\V1;

use App\Models\V1\Photo;
use App\Models\V1\ProductVariant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
	use HasFactory;
	protected $fillable = ["name", "description", "stock", "reviews"];

	public function productVariants()
	{
		return $this->hasMany(ProductVariant::class);
	}

	public function photos() {
		return $this->morphMany(Photo::class, "imageable");
	}
}
