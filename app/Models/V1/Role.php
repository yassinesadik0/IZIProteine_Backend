<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	use HasFactory;
	protected $fillable = ["id", "role"];

	public function users()
	{
		return $this->hasMany(User::class);
	}
}
