<?php

namespace App\Models\V1;

use App\Models\V1\User;
use App\Models\V1\WasteType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Waste extends Model
{
	use HasFactory;

	protected $fillable = ["supplier_id", "moderator_id", "waste_types_id", "quality", "quantity"];
	public function wasteType()
	{
		return $this->belongsTo(WasteType::class, "waste_types_id");
	}

	public function supplier()
	{
		return $this->belongsTo(User::class, "supplier_id");
	}
	public function moderator()
	{
		return $this->belongsTo(User::class, "moderator_id");
	}
}
