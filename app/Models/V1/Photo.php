<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
	use HasFactory;
	protected $fillable = ["file_name", "imageable_id", "imageable_type", "file_path"];

	public function imageable()
	{
		return $this->morphTo();
	}
}
