<?php

namespace App\Models\V1;

use App\Models\V1\User;
use App\Models\V1\Photo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
	use HasFactory;
	protected $fillable = ["title", "link", "description"];

	public function photo() {
		return $this->morphOne(Photo::class, "imageable");
	}
}
