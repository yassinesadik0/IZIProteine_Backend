<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    protected $fillable = ["product_id", "order_id", "quantity", "variant_id"];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function product(){
        return $this->belongsTo(Product::class,"product_id");
    }
    public function variant(){
        return $this->belongsTo(ProductVariant::class, "variant_id");
    }

}
