<?php

namespace App\Models\V1;

use App\Models\V1\Waste;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WasteType extends Model
{
	use HasFactory;
	protected $fillable = ["type"];

	public function wastes()
	{
		return $this->hasMany(Waste::class);
	}
}
