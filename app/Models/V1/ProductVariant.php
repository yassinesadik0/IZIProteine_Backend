<?php

namespace App\Models\V1;

use App\Models\V1\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductVariant extends Model
{
	use HasFactory;

	protected $fillable = ["price", "weight", "product_id"];

	public function product()
	{
		return $this->belongsTo(Product::class, 'product_id');
	}
}
