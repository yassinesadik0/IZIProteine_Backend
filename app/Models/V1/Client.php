<?php

namespace App\Models\V1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $fillable = ["first_name","last_name", "email", "phone", "address", "zip", "city"];


    public function orders()
    {
        return $this->hasMany(Order::class,"client_id");
    }

}
