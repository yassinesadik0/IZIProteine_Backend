<?php

namespace App\Models\V1;

use App\Models\V1\Item;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = ["client_id", "status", "delivery"];



    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function total()
    {
        $items = $this->items;
        $total = 0;
        foreach ($items as $item) {
            $total += $item->variant->price * $item->quantity;
        }
        return $total;
    }
}
