<?php

namespace App\Services;

use App\Models\V1\Photo;
use Illuminate\Support\Facades\Storage;

class PhotoService
{
	public static function update_photo(string $model_name, string $input_name, $instance, $request)
	{
		$file = $request->allFiles()[$input_name];
		$filename = uniqid() . '.' . $file->getClientOriginalExtension();
		$file->storeAs("public/images", $filename);
		$url = url('storage' . '/' . 'images' . '/' . $filename);

		$photo = Photo::where("imageable_id", "=", $instance->id)
			->where("imageable_type", "=", $model_name)
			->first();

		$photo->update([
			"imageable_id" => $instance->id,
			"imageable_type" => $model_name,
			"file_name" => $filename,
			"file_path" => $url,
		]);
	}

	public static function create_photo(string $model_name, string $input_name, $instance, $request)
	{
		$file = $request->allFiles()[$input_name];
		$filename = uniqid() . '.' . $file->getClientOriginalExtension();
		$file->storeAs("public/images", $filename);
		$url = url('storage' . '/' . 'images' . '/' . $filename);

		Photo::create([
			"imageable_id" => $instance->id,
			"imageable_type" => $model_name,
			"file_name" => $filename,
			"file_path" => $url,
		]);
	}

	public static function delete_multiple_photos(string $model_name, $instance)
	{
		$old_photos = Photo::where("imageable_id", "=", $instance->id)
			->where("imageable_type", "=", $model_name)
			->get();

		foreach ($old_photos as $old_photo) {
			Storage::delete("public/images/" . $old_photo->file_name);
			$old_photo->delete();
		}
	}

	public static function delete_photo_from_storage(string $model_name, $instance)
	{
		$old_photo = Photo::where("imageable_id", "=", $instance->id)
			->where("imageable_type", "=", $model_name)
			->first();
		Storage::delete("public/images/" . $old_photo->file_name);
	}

	public static function delete_single_photo(string $model_name, $instance)
	{
		$old_photo = Photo::where("imageable_id", "=", $instance->id)
			->where("imageable_type", "=", $model_name)
			->first();
		Storage::delete("public/images/" . $old_photo->file_name);
		$old_photo->delete();
	}
}
