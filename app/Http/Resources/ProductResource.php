<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use App\Http\Resources\PhotoResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
	public function __construct($resource)
	{
		parent::__construct($resource);
		$this->withoutWrapping();
	}
	
	/**
	 * Transform the resource into an array.
	 *
	 * @return array<string, mixed>
	 */
	public function toArray(Request $request): array
	{
		return [
			"id" => $this->id,
			"name" => $this->name,
			"variants" => ProductVariantResource::collection($this->productVariants),
			"photos" => PhotoResource::collection($this->photos),
			"description" => $this->description,
			"stock" => $this->stock,
			"reviews" => $this->reviews,
		];
	}
}
