<?php

namespace App\Http\Resources;

use App\Models\V1\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        
        return [
            "id"=>$this->id,
            "client"=>ClientResource::make(Client::find($this->client_id)),
            "items"=>ItemResource::collection($this->items),
            "status"=>$this->status,
            "delivery"=> $this->delivery,
            "total"=>$this->total(),
        ];

    }
}
