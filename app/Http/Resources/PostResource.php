<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;

class PostResource extends JsonResource
{
	public function __construct($resource)
	{
		parent::__construct($resource);
		$this->withoutWrapping();
	}

	/**
	 * Transform the resource into an array.
	 *
	 * @return array<string, mixed>
	 */
	public function toArray(Request $request): array
	{
		return [
			"id" => $this->id,
			"title" => $this->title,
			"createdAt" =>  $this->created_at->format("Y-m-d"),
			"link" => $this->link,
			"description" => $this->description,
			"photo" => new PhotoResource($this->photo),
		];
	}
}
