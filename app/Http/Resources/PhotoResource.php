<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PhotoResource extends JsonResource
{

	public function __construct($resource)
	{
		parent::__construct($resource);
		$this->withoutWrapping();
	}

	/**
	 * Transform the resource into an array.
	 *
	 * @return array<string, mixed>
	 */
	public function toArray(Request $request): array
	{
		return [
			"file_name" => $this->file_name,
			"file_path" => $this->file_path,
		];
	}
}
