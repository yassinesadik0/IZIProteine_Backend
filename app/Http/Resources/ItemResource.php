<?php

namespace App\Http\Resources;

use App\Models\V1\Product;
use App\Models\V1\ProductVariant;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id"=>$this->id,
            "order_id"=>$this->order_id,
            "quantity"=>$this->quantity,
            "product"=> ProductResource::make(Product::find($this->product_id)),
            "variant"=> ProductVariantResource::make(ProductVariant::find($this->variant_id)),
        ];
    }
}
