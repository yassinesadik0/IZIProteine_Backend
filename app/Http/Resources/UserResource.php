<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
	public function __construct($resource)
	{
		parent::__construct($resource);
		$this->withoutWrapping();
	}

	/**
	 * Transform the resource into an array.
	 *
	 * @return array<string, mixed>
	 */
	public function toArray(Request $request): array
	{
		return [
			"id" => $this->id,
			"name" => $this->first_name . " " . $this->last_name,
			"role" => $this->role->role,
			"telephone" => $this->telephone,
			"address" => $this->address,
			"email" => $this->email,
		];
	}
}
