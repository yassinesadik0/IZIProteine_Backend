<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariantResource extends JsonResource
{

	public function __construct($resource)
	{
		parent::__construct($resource);
		$this->withoutWrapping();
	}
	
	/**
	 * Transform the resource into an array.
	 *
	 * @return array<string, mixed>
	 */
	public function toArray(Request $request): array
	{
		return [
			"id" => $this->id,
			"product" => $this->product->name,
			"weight" => $this->weight,
			"price" => $this->price,
		];
	}
}
