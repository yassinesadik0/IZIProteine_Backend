<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WasteResource extends JsonResource
{
	public function __construct($resource)
	{
		parent::__construct($resource);
		$this->withoutWrapping();
	}

	/**
	 * Transform the resource into an array.
	 *
	 * @return array<string, mixed>
	 */
	public function toArray(Request $request): array
	{
		return [
			"id" => $this->id,
			"quantity_KG" => $this->quantity,
			"supplier" => $this->supplier->first_name ." ".$this->supplier->last_name,
			"moderator" => $this->moderator->first_name ." ".$this->moderator->last_name,
			"type" => (new WasteTypeResource($this->wasteType))->type,
			"quality" => $this->quality . "/10",
		];
	}
}
