<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateWasteRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 */
	public function authorize(): bool
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
	 */
	public function rules(): array
	{
		return [
			'supplier_id' => ['required', Rule::exists('users', 'id')],
			'moderator_id' => ['required', Rule::exists('users', 'id')],
			'waste_types_id' => ['required', Rule::exists('waste_types', 'id')],
			"quality" => "required|numeric|min:1|max:10",
			"quantity" => "required|numeric"
		];
	}
}
