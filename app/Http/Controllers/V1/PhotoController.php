<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\Photo;
use App\Services\PhotoService;
use App\Http\Controllers\Controller;
use App\Http\Resources\PhotoResource;
use App\Http\Requests\StorePhotoRequest;
use App\Http\Requests\UpdatePhotoRequest;

class PhotoController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$photos = Photo::all();
		return PhotoResource::collection($photos);
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StorePhotoRequest $request)
	{
		$file = $request->file('file_name');
		$filename = uniqid() . '.' . $file->getClientOriginalExtension();
		$path = $request->file('file_name')->storeAs("public/images", $filename);

		Photo::create([
			"imageable_id" => $request->input("imageable_id"),
			"imageable_type" => $request->input("imageable_type"),
			"file_name" => $filename,
			"file_path" => $path,
		]);

	}

	/**
	 * Display the specified resource.
	 */
	public function show(Photo $photo)
	{
		return new PhotoResource($photo);
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdatePhotoRequest $request, Photo $photo)
	{
		$file = $request->file('file_name');
		$filename = uniqid() . '.' . $file->getClientOriginalExtension();
		$request->file('file_name')->storeAs("public/images", $filename);

		$photo->update([
			"imageable_id" => $request->input("imageable_id"),
			"imageable_type" => $request->input("imageable_type"),
			"file_name" => $filename,
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(Photo $photo)
	{
		$photo->deleteOrFail();
	}
}
