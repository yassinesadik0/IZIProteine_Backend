<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\PurshaseRequest;
use App\Models\V1\Client ;
use App\Models\v1\Order;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    //
    public function purchase(PurshaseRequest $request){
        try {
            $requestValidated = $request->validated();
            $client = Client::create([
                "first_name" => $requestValidated->first_name,
                "last_name" => $requestValidated->last_name,
                "email" => $requestValidated->email,
                "phone" => $requestValidated->phone,
                "city" => $requestValidated->city,
                "address" => $requestValidated->address,
                "zip" => $requestValidated->zip
            ]);
            $order = Order::create([
                "client_id" => $client->id,
                "status" => "not confirmed",
                "delivery" => "pending"
            ]);
            foreach ($requestValidated->items as $item) {
                $order->items()->create([
                    "product_id" => $item["product_id"],
                    "quantity" => $item["quantity"],
                    "variant_id" => $item["variant_id"]
                ]);
            }
            return response()->json([
                "message" => "Order created successfully",
                "order" => $order
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "message" => "Error creating order",
                "error" => $th->getMessage()
            ])->status(402);
        }
    }
}
