<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\ProductVariant;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductVariantRequest;
use App\Http\Requests\UpdateProductVariantRequest;
use App\Http\Resources\ProductVariantResource;

class ProductVariantController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$productsVariants = ProductVariant::all();
		return ProductVariantResource::collection($productsVariants);
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StoreProductVariantRequest $request)
	{
		ProductVariant::create($request->validated());
	}

	/**
	 * Display the specified resource.
	 */
	public function show(ProductVariant $productVariant)
	{
		return $productVariant;
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdateProductVariantRequest $request, ProductVariant $productVariant)
	{
		$productVariant->update($request->validated());
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(ProductVariant $productVariant)
	{
		$productVariant->deleteOrFail();
	}
}
