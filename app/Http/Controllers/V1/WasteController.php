<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\Waste;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreWasteRequest;
use App\Http\Requests\UpdateWasteRequest;
use App\Http\Resources\WasteResource;

class WasteController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$wastes = Waste::all();
		return WasteResource::collection($wastes);
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StoreWasteRequest $request)
	{
		$waste = Waste::create($request->validated());
		return new WasteResource($waste);
	}

	/**
	 * Display the specified resource.
	 */
	public function show(Waste $waste)
	{
		return $waste;
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdateWasteRequest $request, Waste $waste)
	{
		$waste->update($request->validated());
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(Waste $waste)
	{
		$waste->deleteOrFail();
	}
}
