<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\Photo;
use App\Models\V1\Product;
use App\Services\PhotoService;
use App\Models\V1\ProductVariant;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$products = Product::with("photos")->get();
		return ProductResource::collection($products);
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StoreProductRequest $request)
	{
		$product = Product::create($request->only(["name", "description", "stock"]));

		$variants = $request->variants;

		foreach ($variants as $variant) {
			ProductVariant::create([
				"weight" => $variant["weight"],
				"price" => $variant["price"],
				"product_id" => $product->id,
			]);
		}

		$files = $request->allFiles()["photos"];

		foreach ($files as $file) {
			$filename = uniqid() . '.' . $file->getClientOriginalExtension();
			$file->storeAs("public/images", $filename);
			$url = url('storage' . '/' . 'images' . '/' . $filename);

			Photo::create([
				"imageable_id" => $product->id,
				"imageable_type" => Product::class,
				"file_name" => $filename,
				"file_path" => $url,
			]);
		}
	}

	/**
	 * Display the specified resource.
	 */
	public function show(Product $product)
	{
		return new ProductResource($product);
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdateProductRequest $request, Product $product)
	{
		$files = $request->allFiles()["photos"];
		
		DB::transaction(function () use ($product, $request, $files) {
			$product->update($request->validated());

			PhotoService::delete_multiple_photos(Product::class, $product);

			foreach ($files as $file) {
				$filename = uniqid() . '.' . $file->getClientOriginalExtension();
				$file->storeAs("public/images", $filename);
				$url = url('storage' . '/' . 'images' . '/' . $filename);

				Photo::create([
					"imageable_id" => $product->id,
					"imageable_type" => Product::class,
					"file_name" => $filename,
					"file_path" => $url,
				]);
			}
		});
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(Product $product)
	{
		$photos = Photo::where("imageable_id", $product->id)->get();

		DB::transaction(function () use ($product, $photos) {
			$product->productVariants()->delete();
			foreach ($photos as $photo) {
				$photo->deleteOrFail();
			}
			$product->deleteOrFail();
		});
	}
}
