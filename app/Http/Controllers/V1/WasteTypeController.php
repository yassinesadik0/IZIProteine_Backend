<?php

namespace App\Http\Controllers\V1;

use App\Http\Resources\WasteTypeResource;
use App\Models\V1\WasteType;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreWasteTypeRequest;
use App\Http\Requests\UpdateWasteTypeRequest;

class WasteTypeController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$types = WasteType::all();
		return WasteTypeResource::collection($types);
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StoreWasteTypeRequest $request)
	{
		WasteType::create($request->validated());
	}

	/**
	 * Display the specified resource.
	 */
	public function show(WasteType $wasteType)
	{
		return new WasteTypeResource($wasteType);
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdateWasteTypeRequest $request, WasteType $wasteType)
	{
		$wasteType->update($request->validated());
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(WasteType $wasteType)
	{
		$wasteType->delete();
	}
}
