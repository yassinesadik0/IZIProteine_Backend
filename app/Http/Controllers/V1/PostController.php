<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\Post;
use App\Models\V1\Photo;
use App\Services\PhotoService;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$posts = Post::with("photo")->get();
		return PostResource::collection($posts);
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StorePostRequest $request)
	{
		DB::transaction(function () use ($request) {
			$post = Post::create($request->validated());
			PhotoService::create_photo(Post::class, "photo", $post, $request);
		});
	}

	/**
	 * Display the specified resource.
	 */
	public function show(Post $post)
	{
		return new PostResource($post);
	}

	/** 
	 * Update the specified resource in storage.
	 */
	public function update(UpdatePostRequest $request, Post $post)
	{
		DB::transaction(function () use ($request, $post) {
			if ($post->photo) {
				PhotoService::delete_photo_from_storage(Post::class, $post);
				PhotoService::update_photo(Post::class, "photo", $post, $request);
			}
			$post->update($request->validated());
		});
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(Post $post)
	{
		DB::transaction(function () use ($post) {
			if ($post->photo) {
				PhotoService::delete_single_photo(Post::class, $post);
			} 
			$post->deleteOrFail();
		});
	}
}
