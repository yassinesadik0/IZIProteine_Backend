<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\Role;
use App\Models\V1\User;
use App\Models\V1\Photo;
use Illuminate\Http\Request;
use App\Services\PhotoService;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$users = User::all();
		return UserResource::collection($users);
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StoreUserRequest $request)
	{
		$user = User::create($request->validated());
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		PhotoService::create_photo(User::class, "photo", $user, $request);
	}

	/**
	 * Display the specified resource.
	 */
	public function show(User $user)
	{
		return new UserResource($user);
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdateUserRequest $request, User $user)
	{
		$user->update($request->validated());
		PhotoService::delete_single_photo(User::class, $user);
		PhotoService::create_photo(User::class, "photo", $user, $request);
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(User $user)
	{
		DB::transaction(function() use ($user) {
			if ($user->photo) {
				PhotoService::delete_single_photo(User::class, $user);
			}
			$user->deleteOrFail();
		});
	}


	public function login(Request $request)
	{
		$credentials = $request->only([
			"email",
			"password"
		]);

		if (!auth()->attempt($credentials)) {
			return response()->json(['status' => 'unauthorized']);
		}

		$user = auth()->user();
		$token = $user->createToken("authToken")->plainTextToken;
		$role = Role::where("id", $user->role_id)->first()->role;
		$user_img = optional(Photo::where("imageable_id", $user->id)->first())->file_path;

		$user = [
			"id" => $user->id,
			"first_name" => $user->first_name,
			"last_name" => $user->last_name,
			"email" => $user->email,
			"photo" => $user_img,
			"role" => $role,
		];

		return response()->json([
			"user" => $user,
			"status" => "authorized",
			"token" => $token
		]);
	}

	public function checkToken(Request $request)
	{
		if ($request->hasHeader('Authorization')) {
			$token = $request->header('Authorization');
			$token = str_replace('Bearer ', '', $token);

			if (auth()->guard('sanctum')->check()) {
				return response()->json([
					"status" => "authorized",
					
				]);
			}
			return response()->json([
				"status" => "expired",
			]);
		}
		return response()->json([
			"status" => "unauthorized",
			"message" => "login"
		]);
	}

	public function register(StoreUserRequest $request)
	{
		// $user = $request->validate([
		// 	"first_name" =>  $request->first_name,
		// 	"last_name" =>  $request->last_name,
		// 	"email"  =>  $request->email,
		// 	"password"  =>  $request->password,
		// ]);
		$transaction = DB::transaction(function () use ($request) {
			$user = User::create([
				"first_name" =>  $request->first_name,
				"last_name" =>  $request->last_name,
				"telephone" =>  $request->telephone,
				"address" =>  $request->address,
				"email"  =>  $request->email,
				"role_id"  =>  $request->role_id,
				"password"  =>  bcrypt($request->password),
			]);
			PhotoService::create_photo(User::class, "photo", $user, $request);
			$token = $user->createToken("authToken")->plainTextToken;
			return [
				"user" => $user,
				"authToken" => $token
			];
		});
		return response()->json($transaction);
	}

	public function logout(Request $request)
	{
		$user = $request->user();
		$user->tokens()->delete();
		return response()->json(["status" => "logout"]);
	}
	public function searchUser(Request $request)
	{
		$query = $request->query("role");
		$role = Role::where("role", "=", $query)->first()->id;

		$users = User::where("role_id", "=", $role)->get();

		return UserResource::collection($users);
	}
}
