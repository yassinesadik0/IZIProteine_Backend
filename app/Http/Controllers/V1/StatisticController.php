<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\Statistic;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStatisticRequest;
use App\Http\Requests\UpdateStatisticRequest;
use App\Models\V1\Waste;
use Illuminate\Http\Request;

class StatisticController extends Controller
{
	public function getPublicWasteStatistics()
	{
		$total_waste = Waste::sum("quantity");
		$co2_reduced = $total_waste * 0.3;
		$energy_saved = $total_waste * 250 / $total_waste;

		$statistics = [
			"wasteTreated" => $total_waste,
			"co2Reduced" => $co2_reduced,
			"energySaved" => $energy_saved,
		];
		return response()->json($statistics);
	}

	public function getSupplierRelatedStatistics(Request $request)
	{
		$waste_by_supplier = Waste::where("supplier_id", "=", $request->id)->sum("quantity");
		$co2_reduced = $waste_by_supplier * 0.3;
		$deposited = Waste::where("supplier_id", "=", $request->id)->count();

		$statistics = [
			"wasteTreated" => $waste_by_supplier,
			"co2Reduced" => round($co2_reduced),
			"deposited" => $deposited,
		];
		return response()->json(["stats" => $statistics]);
	}
}
