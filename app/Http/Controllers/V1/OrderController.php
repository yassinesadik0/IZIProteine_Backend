<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\Order;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\V1\Client;

class OrderController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$orders = Order::all();
		return response()->json(OrderResource::collection($orders));
	}

	/**
	 * Show the form for creating a new resource.
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StoreOrderRequest $request)
	{
		// return response()->json($request->validated());

		$requestValidated = $request->validated();
		$client = Client::create([
			"first_name" => $requestValidated["first_name"],
			"last_name" => $requestValidated["last_name"],
			"email" => $requestValidated["email"],
			"phone" => $requestValidated["phone"],
			"city" => $requestValidated["city"],
			"address" => $requestValidated["address"],
			"zip" => $requestValidated["zip"]
		]);
		$order = Order::create([
			"client_id" => $client->id,
			"status" => "not confirmed",
			"delivery" => "in progress"
		]);
		foreach ($requestValidated["items"] as $item) {
			$order->items()->create([
				"product_id" => $item["id"],
				"quantity" => $item["quantity"],
				"variant_id" => $item["variant_id"]
			]);
		}
		return response()->json([
			"message" => "Order created successfully",
			"order" => $order
		]);
	}

	/**
	 * Display the specified resource.
	 */
	public function show(Order $order)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 */
	public function edit(Order $order)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdateOrderRequest $request, Order $order)
	{
		if ($request->input("reject") === "true") {
			try {
				$order->delete();
				return response()->json(["message" => "deleted successfully"]);
			} catch (\Throwable $th) {
				//throw $th;
				return response()->json(["message" => "error occured" . $th->getMessage()]);
			}
		}
		$order->updateOrFail(["status" => $request->input("status"), "delivery" => $request->input("delivery")]);
		return response()->json(["message" => "updated successfully"]);
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(Order $order)
	{
		//
	}
}
