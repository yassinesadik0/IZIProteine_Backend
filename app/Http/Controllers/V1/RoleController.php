<?php

namespace App\Http\Controllers\V1;

use App\Models\V1\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Http\Resources\RoleResource;

class RoleController extends Controller
{
	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		$roles = Role::all();
		return RoleResource::collection($roles);
	}

	/**
	 * Store a newly created resource in storage.
	 */
	public function store(StoreRoleRequest $request)
	{
		Role::create($request->validated());
	}

	/**
	 * Display the specified resource.
	 */
	public function show(Role $role)
	{
		return new RoleResource($role);
	}

	/**
	 * Update the specified resource in storage.
	 */
	public function update(UpdateRoleRequest $request, Role $role)
	{
		$role->update($request->validated());
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public function destroy(Role $role)
	{
		$role->delete();
	}
}
