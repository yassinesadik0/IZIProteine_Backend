<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;
use Database\Seeders\ProductSeeder;
use Database\Seeders\ProductVariantSeeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 */
	public function run(): void
	{
		$this->call(ProductSeeder::class);
		$this->call(PostSeeder::class);
		$this->call(WasteTypeSeeder::class);
		$this->call(WasteSeeder::class);
		$this->call(OrderSeeder::class);
		// $this->call(ProductVariantSeeder::class);

	}
}
