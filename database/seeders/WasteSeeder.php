<?php

namespace Database\Seeders;

use App\Models\V1\Role;
use App\Models\V1\User;
use App\Models\V1\Waste;
use App\Models\V1\WasteType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class WasteSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 */
	public function run(): void
	{
		$supplier_id = Role::where('role', 'Supplier')->first()->id;
		$moderator_id = Role::where('role', 'Moderator')->first()->id;

		$suppliers = User::where('role_id', $supplier_id)->get();
		$moderators = User::where('role_id', $moderator_id)->get();

		Waste::factory(10)->create([
			"supplier_id" => function () use ($suppliers) {
				return Arr::random($suppliers->toArray())["id"];
			},
			"moderator_id" => function () use ($moderators) {
				return Arr::random($moderators->toArray())["id"];
			},
			"waste_types_id" => function () {
				return Arr::random(WasteType::all()->toArray())["id"];
			},
		]);
	}
}
