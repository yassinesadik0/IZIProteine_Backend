<?php

namespace Database\Seeders;

use App\Models\V1\Post;
use App\Models\V1\Role;
use App\Models\V1\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Arr;

class PostSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 */
	public function run(): void
	{
		Role::factory(3)->create();
		$users = User::factory(10)->create([
			"role_id" => function () {
				return Arr::random(Role::all()->toArray())["id"];
			}
		]);
		Post::factory(10)->create();

		// $randomUser = $users->random();

		// $credentials = [
		// 	'email' => $randomUser->email,
		// 	'password' => '123456', // Assuming the default password is 'password'
		// ];

		// Log::info('One user credentials:', $credentials);
	}
}
