<?php

namespace Database\Seeders;

use App\Models\V1\Client;
use App\Models\V1\Item;
use App\Models\V1\Order;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Client::factory(10)->has(
            Order::factory(1)->has(
                Item::factory(5)
            )
        )->create();
    }
}
