<?php

namespace Database\Seeders;

use App\Models\V1\Product;
use App\Models\V1\ProductVariant;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 */
	public function run(): void
	{
		Product::factory(5)->has(ProductVariant::factory(5))->create();
	}
}
