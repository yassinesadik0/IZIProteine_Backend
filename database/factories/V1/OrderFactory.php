<?php

namespace Database\Factories\V1;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\V1\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $status = $this->faker->randomElement(["to confirm", "confirmed"]);
        return [
            "status"=>$status,
            "delivery"=>$status === "to confirm" ?"in progress":$this->faker->randomElement(["in progress", "delivered"]),
        ];
    }
}
