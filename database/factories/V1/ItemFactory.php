<?php

namespace Database\Factories\V1;

use App\Models\V1\Product;
use App\Models\V1\ProductVariant;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Item>
 */
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $product = Product::pluck("id")->random();
        $variant = ProductVariant::where('product_id', $product)->pluck('id')->random();
        return [
            "product_id"=>$product,
            "variant_id"=>$variant,
            "quantity" => random_int(1,100),
        ];
    }
}
