<?php

namespace Database\Factories\V1;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\V1\Product>
 */
class ProductFactory extends Factory
{
	/**
	 * Define the model's default state.
	 *
	 * @return array<string, mixed>
	 */
	public function definition(): array
	{
		return [
			"name" => fake()->lastName(),
			"description" => fake()->realTextBetween(150, 200),
			"stock" => fake()->numberBetween(0, 100),
			"reviews" => fake()->numberBetween(0, 5),
		];
	}
}
