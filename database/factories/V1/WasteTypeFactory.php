<?php

namespace Database\Factories\V1;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\V1\WasteType>
 */
class WasteTypeFactory extends Factory
{
	/**
	 * Define the model's default state.
	 *
	 * @return array<string, mixed>
	 */
	public function definition(): array
	{
		$array = ["Vegetables", "Meat", "Fish", "Fruit", "Leaves"];
		return [
			"type" => fake()->unique()->randomElement($array),
		];
	}
}
