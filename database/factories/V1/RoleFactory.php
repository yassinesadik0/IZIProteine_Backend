<?php

namespace Database\Factories\V1;

use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\V1\Role>
 */
class RoleFactory extends Factory
{
	/**
	 * Define the model's default state.
	 *
	 * @return array<string, mixed>
	 */
	public function definition(): array
	{
		$array = ["Admin", "Moderator", "Supplier"];
		return [
			"role" => fake()->unique()->randomElement($array),
		];
	}
}
