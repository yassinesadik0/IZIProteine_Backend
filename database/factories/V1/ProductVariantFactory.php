<?php

namespace Database\Factories\V1;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\V1\ProductVariant>
 */
class ProductVariantFactory extends Factory
{
	/**
	 * Define the model's default state.
	 *
	 * @return array<string, mixed>
	 */
	public function definition(): array
	{
		return [
			"price" => fake()->numberBetween(50, 300),
			"weight" => fake()->numberBetween(100, 1000)
		];
	}
}
