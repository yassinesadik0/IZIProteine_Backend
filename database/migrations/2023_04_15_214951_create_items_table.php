<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
	/**
	 * Run the migrations.
	 */

	public function up(): void
	{
		Schema::create('items', function (Blueprint $table) {
			$table->id();
			$table->foreignId("product_id")->constrained("products")->onDelete("cascade")->onUpdate("cascade");
			$table->integer("quantity");
			$table->foreignId("variant_id")->constrained("product_variants")->onDelete("cascade")->onUpdate("cascade");
			$table->foreignId("order_id")->constrained("orders")->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		Schema::dropIfExists('items');
	}
};
